

import sun.awt.Symbol;

import java.util.Scanner;

public class MyTicTacToe {

    static char SYMBOL_X = 'X';
    static char SYMBOL_0 = '0';
    static char[] SYMBOLS = new char[]{SYMBOL_X, SYMBOL_0};

    Player player1;

    Player player2;

    char[][] board;

    int size;

    MyTicTacToe(int size, String player1, String player2) {
        this.size = size;
        this.board = new char[size][size];
        this.player1 = new Player(player1, SYMBOL_X);
        this.player2 = new Player(player2, SYMBOL_0);
        initBoard();
    }


    public void initBoard() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                board[i][j] = (char) (48 + size * i + 1 + j);
                int string[][];
            }
        }

    }

    public void showBoard() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(board[i][j] + " ");
            }

            System.out.println();
        }

    }

    public int readMove(Player player) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Jucatorul " + player.name + " muta:");

        // not a valid move yet
        boolean isValid = false;
        int move = -1;

        while (!isValid) {
            // read a move
            move = scanner.nextInt();
            // validate move
            isValid = permitedMove(move);
        }

        return move;
    }

    public boolean permitedMove(int move) {
        // extract coordinates
        int i = (move - 1) / size;
        int j = (move - 1) % size;

        // test value at coordinates
        if (board[i][j] == SYMBOL_X || board[i][j] == SYMBOL_0) {
            System.out.println("That position is already taken, please choose another");
            return false;
        } else {
            return true;
        }
    }

    public void makeMove(Player player, int move) {

        int i = (move - 1) / size;
        int j = (move - 1) % size;
        board[i][j] = player.symbol;
        //if (board != null ){
        //  System.out.println("test");
        //  }
    }


    public boolean isWinLine(Player player, int line) {
        boolean win = true;
        int i = 0;
        while (i < size) {
            if (board[line][i] != player.symbol) {
                win = false;
            }
            i++;
        }
        return win;
    }


    public boolean isWinCol(Player player, int col) {
        boolean win = true;
        int j = 0;
        while (j < size) {
            if (board[j][col] != player.symbol) {
                win = false;
            }
            j++;
        }
        return win;
    }


    public boolean isWinDiag1(Player player) {
        boolean win = true;
        int i = 0;
        while (i < size) {
            if (board[i][i] != player.symbol) {
                win = false;
            }
            i++;
        }
        return win;
    }


    public boolean isWinDiag2(Player player) {
        boolean win = true;
        int j = size;
        while (j < size - 1 - j) {
            if (board[j][j] != player.symbol) {
                win = false;
            }
            j--;
        }

        return win;
    }

    public boolean isWin(Player player, int move) {
        // determine coordonates
        int i = (move - 1) / size;
        int j = (move - 1) % size;
        // test win
        boolean win = false;
        win = isWinLine(player, i);
        if (!win) {
            win = isWinCol(player, j);
        }
        if (!win && i == j) {
            win = isWinDiag1(player);
        }
        if (!win && i == (size - 1 - j)) {
            win = isWinDiag2(player);
        }

        return win;
    }


    public void playGame() {
        int nrMoves = 0;
        Player currentPlayer = player1;
        boolean win = false;
        while (nrMoves < (size * size) && !win) {

            int move = readMove(currentPlayer);
            makeMove(currentPlayer, move);
            nrMoves++;


            showBoard();
            //not testing for first 4 moves on 3 x 3
            if (nrMoves >= 2 * size - 1) {
                win = isWin(currentPlayer, move);
            }
            if (!win) {
                // switch players
                if (currentPlayer == player1) {
                    currentPlayer = player2;
                } else {
                    currentPlayer = player1;
                }
            }
        }
        if (win == false) {
            System.out.println("Remiza!");
        } else {
            System.out.println("Felicitari " + currentPlayer.name + ", ai castigat! Clap! Clap! Clap!");
        }
    }
}